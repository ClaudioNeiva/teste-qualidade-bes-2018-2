package br.ucsal.bes20182.testequalidade.aula09.exemplo2;

public class AlunoBuilder {

	public static Aluno umAlunoAtivo() {
		return new Aluno(SituacaoAluno.ATIVO);
	}

	public static Aluno umAlunoCancelado() {
		return new Aluno(SituacaoAluno.CANCELADO);
	}

}
