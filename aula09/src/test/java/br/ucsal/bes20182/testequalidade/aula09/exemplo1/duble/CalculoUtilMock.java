package br.ucsal.bes20182.testequalidade.aula09.exemplo1.duble;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;

import br.ucsal.bes20182.testequalidade.aula09.exemplo1.CalculoUtil;

// Implementação "de brincadeira" do que seria um mock ... apenas uma brincadeira.
public class CalculoUtilMock extends CalculoUtil {

	// Map<o valor de n, a quantidade de chamadas para aquele valor de n>
	private Map<Integer, Integer> chamadasCalcularFatorial = new HashMap<>();
	
	public Long calcularFatorial(int n) {
		registrarChamadaCalcularFatorial(n);
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		case 3:
			return 6L;
		}
		return null;
	}

	public void verificarChamadaCalcularFatorial(Integer n, Integer qtdChamadasEsperada){
		Integer qtdChamadasAtual = 0;
		if(chamadasCalcularFatorial.containsKey(n)){
			qtdChamadasAtual = chamadasCalcularFatorial.get(n);
		}
		Assert.assertEquals(qtdChamadasEsperada, qtdChamadasAtual);
	}
	
	private void registrarChamadaCalcularFatorial(int n) {
		if (!chamadasCalcularFatorial.containsKey(n)) {
			chamadasCalcularFatorial.put(n, 1);
		} else {
			chamadasCalcularFatorial.put(n, chamadasCalcularFatorial.get(n) + 1);
		}
	}

}
