package br.ucsal.bes20182.testequalidade.aula09.exemplo2.dubles;

import br.ucsal.bes20182.testequalidade.aula09.exemplo2.Aluno;
import br.ucsal.bes20182.testequalidade.aula09.exemplo2.AlunoBuilder;
import br.ucsal.bes20182.testequalidade.aula09.exemplo2.AlunoDAO;

public class AlunoDAOStub extends AlunoDAO {

	// Esta classe traz previsibilidade para o retorno da consulta.

	@Override
	public void salvar(Aluno aluno) {
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		if (matricula.equals(123)) {
			Aluno aluno = AlunoBuilder.umAlunoCancelado();
			aluno.setMatricula(matricula);
			return aluno;
		}
		return null;
	}

}
