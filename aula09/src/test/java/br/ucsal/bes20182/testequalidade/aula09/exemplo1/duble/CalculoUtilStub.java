package br.ucsal.bes20182.testequalidade.aula09.exemplo1.duble;

import br.ucsal.bes20182.testequalidade.aula09.exemplo1.CalculoUtil;

public class CalculoUtilStub extends CalculoUtil {
	
	public Long calcularFatorial(int n) {
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		case 3:
			return 6L;
		}
		return null;
	}

}
