package br.ucsal.bes20182.testequalidade.aula09.exemplo2.dubles;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.testequalidade.aula09.exemplo2.Aluno;
import br.ucsal.bes20182.testequalidade.aula09.exemplo2.AlunoDAO;

public class AlunoDAOFake extends AlunoDAO {

	private List<Aluno> alunos = new ArrayList<>();

	// Esta classe permitiria que os testes rodassem, mesmo com o SGBD fora do
	// ar.

	@Override
	public void salvar(Aluno aluno) {
		// O salvar, por estar um um duble, deve sobrescrever o comportamento do
		// AlunoDAO real, pois este �ltimo depende do SGBD.
		alunos.add(aluno);
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		for (Aluno aluno : alunos) {
			if (aluno.getMatricula().equals(matricula)) {
				return aluno;
			}
		}
		return null;
	}
}
