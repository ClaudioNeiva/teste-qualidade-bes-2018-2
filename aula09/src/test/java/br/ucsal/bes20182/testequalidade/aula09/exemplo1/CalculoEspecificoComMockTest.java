package br.ucsal.bes20182.testequalidade.aula09.exemplo1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.aula09.exemplo1.duble.CalculoUtilMock;

public class CalculoEspecificoComMockTest {

	private CalculoEspecifico calculoEspecifico;
	private CalculoUtil calculoUtilMock;

	@Before
	public void setup() {
		calculoUtilMock = new CalculoUtilMock();
		calculoEspecifico = new CalculoEspecifico(calculoUtilMock);
	}

	@Test
	public void testarCalculoE3() {
		int n = 3;
		Double eEsperado = 2.66;
		Double eAtual = calculoEspecifico.calcularE(n);
		Assert.assertEquals(eEsperado, eAtual, 0.01);
		((CalculoUtilMock) calculoUtilMock).verificarChamadaCalcularFatorial(2, 3);
	}

}
