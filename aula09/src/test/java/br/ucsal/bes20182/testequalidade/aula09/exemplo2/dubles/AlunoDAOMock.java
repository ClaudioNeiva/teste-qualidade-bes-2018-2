package br.ucsal.bes20182.testequalidade.aula09.exemplo2.dubles;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;

import br.ucsal.bes20182.testequalidade.aula09.exemplo2.Aluno;
import br.ucsal.bes20182.testequalidade.aula09.exemplo2.AlunoBuilder;
import br.ucsal.bes20182.testequalidade.aula09.exemplo2.AlunoDAO;

public class AlunoDAOMock extends AlunoDAO {

	// Esta classe traz al�m da previsibilidade para o retorno da consulta, a
	// rastreabilidade para as chamadas m�todo.

	// Map<nome-do-metodo, Map<object passado com par�metro pro m�todo,
	// qtd-chamadas para o m�todo com o par�metro>
	private Map<String, Map<Object, Integer>> chamadasMetodo = new HashMap<>();

	@Override
	public void salvar(Aluno aluno) {
		registrarChamada("salvar", aluno);
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		registrarChamada("encontrarPorMatricula", matricula);
		if (matricula.equals(123)) {
			Aluno aluno = AlunoBuilder.umAlunoCancelado();
			aluno.setMatricula(matricula);
			return aluno;
		}
		return null;
	}

	public void verificarChamada(String nomeMetodo, Object object, Integer qtdChamadasEsperada) {
		Integer qtdChamadasAtual = 0;
		if (chamadasMetodo.containsKey(nomeMetodo)) {
			if (chamadasMetodo.get(nomeMetodo).containsKey(object)) {
				qtdChamadasAtual = chamadasMetodo.get(nomeMetodo).get(object);
			}
		}
		Assert.assertEquals(qtdChamadasEsperada, qtdChamadasAtual);
	}

	private void registrarChamada(String nomeMetodo, Object object) {
		if (!chamadasMetodo.containsKey(nomeMetodo)) {
			chamadasMetodo.put(nomeMetodo, new HashMap<>());
		}
		Map<Object, Integer> mapaAtual = chamadasMetodo.get(nomeMetodo);
		if (!mapaAtual.containsKey(object)) {
			mapaAtual.put(object, 1);
		} else {
			mapaAtual.put(object, mapaAtual.get(object) + 1);
		}
	}

}
