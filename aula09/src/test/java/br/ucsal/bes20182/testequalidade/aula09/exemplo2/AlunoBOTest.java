package br.ucsal.bes20182.testequalidade.aula09.exemplo2;

import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.aula09.exemplo2.dubles.AlunoDAOMock;

public class AlunoBOTest {

	private AlunoDAO alunoDAO;
	private AlunoBO alunoBO;

	@Before
	public void setup() {
		alunoDAO = new AlunoDAOMock();
		alunoBO = new AlunoBO(alunoDAO);
	}

	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Aluno aluno1 = AlunoBuilder.umAlunoAtivo();
		alunoBO.atualizar(aluno1);
		// Verificar se o m�todo salvar do AlunoDAO foi chamado para o object
		// aluno1.
		((AlunoDAOMock) alunoDAO).verificarChamada("salvar", aluno1, 1);
	}

	@Test
	public void testarAtualizacaoAlunosCancelados() {
		Aluno aluno1 = AlunoBuilder.umAlunoCancelado();
		alunoBO.atualizar(aluno1);
		// Verificar se o m�todo salvar do AlunoDAO foi chamado para o object
		// aluno1.
		((AlunoDAOMock) alunoDAO).verificarChamada("salvar", aluno1, 0);
	}
}
