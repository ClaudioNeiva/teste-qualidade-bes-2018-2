package br.ucsal.bes20182.testequalidade.aula09.exemplo1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.aula09.exemplo1.duble.CalculoUtilStub;

public class CalculoEspecificoTest {

	private CalculoEspecifico calculoEspecifico;

	@Before
	public void setup() {
		CalculoUtil calculoUtil = new CalculoUtilStub();
		calculoEspecifico = new CalculoEspecifico(calculoUtil);
	}

	@Test
	public void testarCalculoE3() {
		int n = 3;
		Double eEsperado = 2.66;
		Double eAtual = calculoEspecifico.calcularE(n);
		Assert.assertEquals(eEsperado, eAtual, 0.01);
	}

}
