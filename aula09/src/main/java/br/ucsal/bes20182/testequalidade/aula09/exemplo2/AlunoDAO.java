package br.ucsal.bes20182.testequalidade.aula09.exemplo2;

public class AlunoDAO {

	// Esta classe depender um SGBD funcionando.

	public void salvar(Aluno aluno) {
		// Aqui deveria existir a implementa��o real para salvar o aluno no
		// banco de dados.
	}

	public Aluno encontrarPorMatricula(Integer matricula) {
		// Aqui o c�digo deveria encontrar o aluno no banco de dados.
		return new Aluno();
	}

}
