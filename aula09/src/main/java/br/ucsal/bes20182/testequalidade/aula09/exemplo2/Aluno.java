package br.ucsal.bes20182.testequalidade.aula09.exemplo2;

public class Aluno {

	// Aqui deveria existir a implementação da classe alunos, com seus atributos
	// e métodos.

	private Integer matricula;

	private SituacaoAluno situacao;

	public Aluno() {
	}

	public Aluno(SituacaoAluno situacao) {
		super();
		this.situacao = situacao;
	}

	public SituacaoAluno getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAluno situacao) {
		this.situacao = situacao;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

}
