package br.ucsal.bes20182.testequalidade.aula09.exemplo2;

public class AlunoBO {

	private AlunoDAO alunoDAO;

	public AlunoBO(AlunoDAO alunoDAO) {
		this.alunoDAO = alunoDAO;
	}

	public void atualizar(Aluno aluno) {
		if (SituacaoAluno.ATIVO.equals(aluno.getSituacao())) {
			alunoDAO.salvar(aluno);
		}
	}

}
