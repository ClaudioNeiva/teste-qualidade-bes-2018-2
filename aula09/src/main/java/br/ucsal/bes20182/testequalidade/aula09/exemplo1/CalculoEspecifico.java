package br.ucsal.bes20182.testequalidade.aula09.exemplo1;

public class CalculoEspecifico {
	
	public CalculoUtil calculorUtil;
	
	public CalculoEspecifico(CalculoUtil calculoUtil){
		this.calculorUtil = calculoUtil;
	}
	
	public Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			e += 1d / calculorUtil.calcularFatorial(i);
		}
		return e;
	}

}
