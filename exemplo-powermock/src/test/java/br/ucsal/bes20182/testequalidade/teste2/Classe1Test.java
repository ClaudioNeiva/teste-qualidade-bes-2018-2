package br.ucsal.bes20182.testequalidade.teste2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Classe1.class })
public class Classe1Test {

	@Test
	public void teste1() throws Exception {

		Classe1 objeto1 = new Classe1();
		Classe1 spy1 = PowerMockito.spy(objeto1);

		PowerMockito.mockStatic(Classe1.class);

		PowerMockito.when(Classe1.metodo1()).thenReturn(10);
		PowerMockito.when(Classe1.class, "metodo2").thenReturn(20);
		PowerMockito.when(spy1, "metodo3").thenReturn(30);

		spy1.metodo4();

		PowerMockito.verifyPrivate(spy1).invoke("metodo3");
		PowerMockito.verifyPrivate(Classe1.class).invoke("metodo2");

		PowerMockito.verifyStatic();
		Classe1.metodo1();
		PowerMockito.verifyNoMoreInteractions(Classe1.class);
	}

	@Test
	public void teste2() throws Exception {

		Classe1 objeto1 = new Classe1();
		Classe1 spy1 = PowerMockito.spy(objeto1);

		PowerMockito.mockStatic(Classe1.class);

		PowerMockito.when(Classe1.metodo1()).thenReturn(100);
		PowerMockito.when(Classe1.class, "metodo2").thenReturn(200);
		PowerMockito.when(spy1, "metodo3").thenReturn(300);

		Whitebox.invokeMethod(spy1, "metodo5");

		PowerMockito.verifyPrivate(spy1).invoke("metodo3");
		PowerMockito.verifyPrivate(Classe1.class).invoke("metodo2");

		PowerMockito.verifyStatic();
		Classe1.metodo1();
		PowerMockito.verifyNoMoreInteractions(Classe1.class);
	}

}
