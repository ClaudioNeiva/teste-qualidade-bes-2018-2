package br.ucsal.bes20182.testequalidade.teste2;

public class Classe1 {

	private int atributo1;

	private static int atributo2;

	public static int metodo1() {
		return 1;
	}

	private static int metodo2() {
		return 2;
	}

	private int metodo3() {
		return 3;
	}

	public void metodo4() {
		System.out.println("metodo1()=" + metodo1());
		System.out.println("metodo2()=" + metodo2());
		System.out.println("metodo3()=" + metodo3());
	}

	private void metodo5() {
		System.out.println("metodo1()=" + metodo1());
		System.out.println("metodo2()=" + metodo2());
		System.out.println("metodo3()=" + metodo3());
	}

	public void metodo6() {
		System.out.println("atributo1 = " + atributo1);
		System.out.println("atributo2 = " + atributo2);
	}
}
