package br.ucsal.bes20182.testequalidade.atividade2;

import org.junit.Assert;
import org.junit.Test;

public class Atividade2Test {

	@Test
	public void testarFatorial3() {
		// Dados de entrada
		Integer n = 3;

		// Sa�da esperada
		Long fatorialEsperado = 6L;

		// Execu��o do m�todo a ser testado e coleta do resultado atual
		Long fatorialAtual = Atividade2.calcularFatorial(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial0() {
		// Dados de entrada
		Integer n = 0;

		// Sa�da esperada
		Long fatorialEsperado = 1L;

		// Execu��o do m�todo a ser testado e coleta do resultado atual
		Long fatorialAtual = Atividade2.calcularFatorial(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
}
