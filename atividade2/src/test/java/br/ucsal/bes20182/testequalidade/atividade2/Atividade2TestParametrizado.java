package br.ucsal.bes20182.testequalidade.atividade2;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class Atividade2TestParametrizado {

	@Parameters(name="{index} - calcularFatorial({0})={1}")
	public static Collection<Object[]> obterCasosTeste() {
		return Arrays.asList(new Object[][] { { 0, 1L }, { 3, 6L }, { 5, 120L } });
	}
	
	@Parameter
	public Integer n;
	
	@Parameter(1)
	public Long fatorialEsperado;

	@Test
	public void testarFatorial3() {
		// Execu��o do m�todo a ser testado e coleta do resultado atual
		Long fatorialAtual = Atividade2.calcularFatorial(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
