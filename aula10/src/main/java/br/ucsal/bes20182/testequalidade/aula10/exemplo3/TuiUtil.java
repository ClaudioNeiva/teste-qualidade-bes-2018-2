package br.ucsal.bes20182.testequalidade.aula10.exemplo3;

import java.util.Scanner;

public class TuiUtil {

	public Scanner scanner = new Scanner(System.in);

	public String obterNomeCompleto() {
		System.out.println("Informe o nome:");
		String nome = scanner.nextLine();
		System.out.println("Informe o sobrenome:");
		String sobrenome = scanner.nextLine();
		return nome + " " + sobrenome;
	}

	public void exibirMensagem(String mensagem){
		System.out.println("Bom dia!");
		System.out.println(mensagem);
	}
	
}
