package br.ucsal.bes20182.testequalidade.aula10.exemplo2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class AlunoBOTest {

	private AlunoDAO alunoDAOMock;
	private DateUtil dateUtilMock;
	private AlunoBO alunoBO;

	@Before
	public void setup() {
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		dateUtilMock = Mockito.mock(DateUtil.class);
		alunoBO = new AlunoBO(alunoDAOMock, dateUtilMock);
	}

	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().build();
		alunoBO.atualizar(aluno1);
		// Verificar se o m�todo salvar do AlunoDAO foi chamado para o object
		// aluno1.
		Mockito.verify(alunoDAOMock).salvar(aluno1);
	}

	@Test
	public void testarCalculoIdadeAluno1() {

		Aluno aluno1 = AlunoBuilder.umAluno().nascidoEm(2003).build();

		// O par�metro 30, "ensina" ao alunoDAOMock que ele s� deve retornar a
		// inst�ncia aluno1 se ao chamar o m�todo encontrarPorMatricula for
		// passado como par�metro a matr�cula 30.
		Mockito.doReturn(aluno1).when(alunoDAOMock).encontrarPorMatricula(30);

		// Aqui vamos "ensinar" ao dateUtilMock a sempre que for feita uma
		// chamada ao m�todo obterAnoAtual, deve ser retornado 2018. Desta
		// forma, no ano que vem, um aluno nascido em 2003 vai continuar tendo
		// 15 anos e o teste n�o vai quebrar simplesmente pela passagem do ano.
		Mockito.doReturn(2018).when(dateUtilMock).obterAnoAtual();

		Integer idadeAtual = alunoBO.calcularIdade(30);
		Integer idadeEsperada = 15;
		Assert.assertEquals(idadeEsperada, idadeAtual);

		// Essa verifica��o � opcional, pois j� � poss�vel concluir o correto
		// funcionamento do m�todo calcularIdade a partir do retorno do mesmo.
		Mockito.verify(alunoDAOMock).encontrarPorMatricula(30);
	}

	@Test
	public void testarCalculoIdadeAluno2() {

		Aluno aluno1 = AlunoBuilder.umAluno().nascidoEm(2003).build();

		// O par�metro Mockito.anyInt(), "ensina" ao alunoDAOMock ao chamar o
		// m�todo encontrarPorMatricula, com qualquer valor para o par�metro
		// matr�cula, deve ser retornado o objeto aluno1. Pode ser o 654 da
		// chamada abaixo, ou qualquer outro valor inteiro.
		Mockito.doReturn(aluno1).when(alunoDAOMock).encontrarPorMatricula(Mockito.anyInt());

		// Aqui vamos "ensinar" ao dateUtilMock a sempre que for feita uma
		// chamada ao m�todo obterAnoAtual, deve ser retornado 2018. Desta
		// forma, no ano que vem, um aluno nascido em 2003 vai continuar tendo
		// 15 anos e o teste n�o vai quebrar simplesmente pela passagem do ano.
		Mockito.doReturn(2018).when(dateUtilMock).obterAnoAtual();
		
		Integer idadeAtual = alunoBO.calcularIdade(654);
		Integer idadeEsperada = 15;
		Assert.assertEquals(idadeEsperada, idadeAtual);

	}
}
