package br.ucsal.bes20182.testequalidade.aula10.exemplo3;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class TuiUtilTest {

	@Test
	public void testarObterNomeCompleto() {
		TuiUtil tuiUtil = new TuiUtil();

		// Dados de entrada
		String nome = "Claudio";
		String sobrenome = "Neiva";

		// Criar um objeto Scanner mock para substituir a implementa��o real do
		// Scanner.
		Scanner scannerMock = Mockito.mock(Scanner.class);
		tuiUtil.scanner = scannerMock;

		Mockito.doReturn(nome).doReturn(sobrenome).when(scannerMock).nextLine();

		// Sa�da esperada
		String nomeCompletoEsperado = "Claudio Neiva";

		// Chamada do m�todo em teste e obten��o do resultado atual
		String nomeCompletoAtual = tuiUtil.obterNomeCompleto();

		// Compara��o do resultado esperado x resultado atual
		Assert.assertEquals(nomeCompletoEsperado, nomeCompletoAtual);
	}

	@Test
	public void testarExibirMensagem() {
		String mensagem = "Tem que estudar e muito!";

		TuiUtil tuiUtil = new TuiUtil();

		// Preparar os mocks. Observe que n�o � necess�rio "ensinar" coisas para
		// o printStreamMock, pois � irrelevanto o que ele faz ao ser chamado o
		// m�todo println.
		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		System.setOut(printStreamMock);

		// Agora que o System.out aponta para o printStreamMock, podemos
		// executar o m�todo exibirMensagem, pois as chamadas println ser�o
		// feitas n�o mais para a console e sim para o objeto mocado, permitindo
		// um futuro verify.
		tuiUtil.exibirMensagem(mensagem);
		
		// Verificar se ocorreram as chamadas ao m�todo println.
		Mockito.verify(printStreamMock).println("Bom dia!");
		Mockito.verify(printStreamMock).println(mensagem);
	}

}
