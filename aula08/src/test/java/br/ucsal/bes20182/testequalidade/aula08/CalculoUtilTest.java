package br.ucsal.bes20182.testequalidade.aula08;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CalculoUtilTest {

	public CalculoUtil calculoUtil;
	
	@Before
	public void setup(){
		calculoUtil = new CalculoUtil();
	}
	
	@Test
	public void testarFatorial2(){
		int n=2;
		Long fatorialEsperado = 2L;
		Long fatorialAtual = calculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
	
}
