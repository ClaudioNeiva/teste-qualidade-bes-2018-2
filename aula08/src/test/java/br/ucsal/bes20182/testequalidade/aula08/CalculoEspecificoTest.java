package br.ucsal.bes20182.testequalidade.aula08;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.aula08.duble.CalculoUtilDuble;

public class CalculoEspecificoTest {

	private CalculoEspecifico calculoEspecifico;

	@Before
	public void setup() {
		CalculoUtil calculoUtil = new CalculoUtilDuble();
		calculoEspecifico = new CalculoEspecifico(calculoUtil);
	}

	@Test
	public void testarCalculoE3() {
		int n = 3;
		Double eEsperado = 2.66;
		Double eAtual = calculoEspecifico.calcularE(n);
		Assert.assertEquals(eEsperado, eAtual, 0.01);
	}

}
