package br.ucsal.bes20182.testequalidade.aula08;

public class Exemplo {

	public static void main(String[] args) {
		CalculoUtil calculoUtil = new CalculoUtil();
		CalculoEspecifico calculoEspecifico = new CalculoEspecifico(calculoUtil);
		Integer n = 3;
		Double e = calculoEspecifico.calcularE(n);
		System.out.println("e(" + n + ")=" + e);
	}

}
